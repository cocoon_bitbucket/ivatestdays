echo "start or restart dockerui"
docker kill dockerui
sleep 3
docker rm dockerui
docker run -d -p 9000:9000 -v /var/run/docker.sock:/docker.sock \
--name dockerui abh1nav/dockerui:latest -e="/docker.sock"
docker ps