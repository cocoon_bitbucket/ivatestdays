ivaconsole
==========

console docker image based on cocoon/ivatools


composition
========
debian:wheezy
	cocoon/python
		cocoon/ivatools


use
===

```
docker -ti --volumes-from iva_data_1 cocoon/ivatools jenkins
```
where iva_data_1 is a data container supplying the /jenkins and /tests volumes