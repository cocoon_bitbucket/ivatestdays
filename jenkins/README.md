ivajenkins
==========

jenkins docker image based on cocoon/ivatools


composition
========
debian:wheezy
	cocoon/python
		cocoon/ivatools


use
===

```
docker -d -rm --volumes-from iva_data_1 cocoon/ivajenkins
```
where iva_data_1 is a data container supplying the /jenkins and /tests volumes